﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Centroids
{
    public partial class imageForm : Form
    {
        List<Point> centroids;
        string route;   

        public imageForm()
        {
            route = "";
            centroids = new List<Point>();
            InitializeComponent();
            titleTextBox.Text = "";
        }

        private void selectImageButton_Click(object sender, EventArgs e)
        {
            this.selectImageFileDialog.ShowDialog();
            route = selectImageFileDialog.FileName;
            try
            {
                if (!System.IO.File.Exists(route))
                {
                    MessageBox.Show("Ingrese un archivo para verificar primero");
                    return;
                }
                else
                {
                    this.imagePictureBox.Load(route);
                    titleTextBox.Text = "";
                    centersListBox.Items.Clear();
                    graphTextBox.Clear();
                    weightTextBox.Clear();
                }
            }
            catch (ArgumentException)
            {
                MessageBox.Show("Ingrese una imagen para poder analizar");
            }
        }

        private void showCentroids_Click(object sender, EventArgs e)
        {
            try
            {
                centersListBox.Items.Clear();
                Bitmap originalImage = new Bitmap(route);
                ImageProcessing imageProcessing = new ImageProcessing(originalImage);
                centroids = imageProcessing.FindCircles();
                titleTextBox.Text = "Puntos Encontrados";
                foreach (Point point in centroids)
                {
                    centersListBox.Items.Add(point);
                }
                Graph graph = new Graph(centroids);
                imagePictureBox.Image = imageProcessing.GetPicture();
            }
            catch (ArgumentException)
            {
                MessageBox.Show("Ingrese una imagen para verificar primero");
            }
        }

        private void graphButton_Click(object sender, EventArgs e)
        {
            showCentroids_Click(sender, e);
            try
            {
                Graph graph = new Graph(centroids);
                Bitmap graphImage = new Bitmap(route);
                Graphics graphics = Graphics.FromImage(graphImage);
                Pen blackPen = new Pen(Color.Black, 6);
                foreach (Vertex vertex in graph.getVertexList())
                {
                    foreach (Edge edge in vertex.getEdgeList())
                    {
                        Point initialPoint = vertex.getPoint();
                        Point finalPoint = edge.getVertex().getPoint();
                        graphics.DrawLine(blackPen, initialPoint, finalPoint);
                    }
                    imagePictureBox.Image = graphImage;
                }

                Algorithm algorithm = new Algorithm();

                List<Vertex> list = algorithm.lighterWay(graph);
                Pen bluePen = new Pen(Color.Blue, 6);
                for (int i = 0; i < 3; i++)
                {
                    graphics.DrawLine(bluePen, list[i].getPoint(), list[i + 1].getPoint());
                }
                graphTextBox.Clear();
                weightTextBox.Clear();
                weightTextBox.Text = "Peso final de la ruta: \n" + algorithm.leastHeavy.ToString();
                graphTextBox.Text = Algorithm.graphInfo;
            }
            catch (ArgumentException){}
        }
    }
}
