﻿namespace Centroids
{
    partial class imageForm
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.imagePictureBox = new System.Windows.Forms.PictureBox();
            this.showCentroidsButton = new System.Windows.Forms.Button();
            this.selectImageButton = new System.Windows.Forms.Button();
            this.centersListBox = new System.Windows.Forms.ListBox();
            this.titleTextBox = new System.Windows.Forms.TextBox();
            this.selectImageFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.grafoButton = new System.Windows.Forms.Button();
            this.graphTextBox = new System.Windows.Forms.RichTextBox();
            this.weightTextBox = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.imagePictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // imagePictureBox
            // 
            this.imagePictureBox.Location = new System.Drawing.Point(12, 71);
            this.imagePictureBox.Name = "imagePictureBox";
            this.imagePictureBox.Size = new System.Drawing.Size(553, 477);
            this.imagePictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imagePictureBox.TabIndex = 0;
            this.imagePictureBox.TabStop = false;
            // 
            // showCentroidsButton
            // 
            this.showCentroidsButton.Location = new System.Drawing.Point(567, 13);
            this.showCentroidsButton.Name = "showCentroidsButton";
            this.showCentroidsButton.Size = new System.Drawing.Size(118, 48);
            this.showCentroidsButton.TabIndex = 1;
            this.showCentroidsButton.Text = "Mostrar Centroides";
            this.showCentroidsButton.UseVisualStyleBackColor = true;
            this.showCentroidsButton.Click += new System.EventHandler(this.showCentroids_Click);
            // 
            // selectImageButton
            // 
            this.selectImageButton.Location = new System.Drawing.Point(295, 12);
            this.selectImageButton.Name = "selectImageButton";
            this.selectImageButton.Size = new System.Drawing.Size(118, 50);
            this.selectImageButton.TabIndex = 2;
            this.selectImageButton.Text = "Seleccionar Imagen";
            this.selectImageButton.UseVisualStyleBackColor = true;
            this.selectImageButton.Click += new System.EventHandler(this.selectImageButton_Click);
            // 
            // centersListBox
            // 
            this.centersListBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.centersListBox.FormattingEnabled = true;
            this.centersListBox.ItemHeight = 17;
            this.centersListBox.Location = new System.Drawing.Point(598, 95);
            this.centersListBox.Name = "centersListBox";
            this.centersListBox.Size = new System.Drawing.Size(124, 221);
            this.centersListBox.TabIndex = 3;
            // 
            // titleTextBox
            // 
            this.titleTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.titleTextBox.Location = new System.Drawing.Point(598, 71);
            this.titleTextBox.Name = "titleTextBox";
            this.titleTextBox.Size = new System.Drawing.Size(124, 18);
            this.titleTextBox.TabIndex = 4;
            this.titleTextBox.Text = "Puntos Encontrados";
            this.titleTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // selectImageFileDialog
            // 
            this.selectImageFileDialog.FileName = "openFileDialog1";
            // 
            // grafoButton
            // 
            this.grafoButton.Location = new System.Drawing.Point(12, 10);
            this.grafoButton.Name = "grafoButton";
            this.grafoButton.Size = new System.Drawing.Size(118, 50);
            this.grafoButton.TabIndex = 5;
            this.grafoButton.Text = "Grafo y Ruta";
            this.grafoButton.UseVisualStyleBackColor = true;
            this.grafoButton.Click += new System.EventHandler(this.graphButton_Click);
            // 
            // graphTextBox
            // 
            this.graphTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.graphTextBox.Location = new System.Drawing.Point(12, 554);
            this.graphTextBox.Name = "graphTextBox";
            this.graphTextBox.Size = new System.Drawing.Size(710, 145);
            this.graphTextBox.TabIndex = 6;
            this.graphTextBox.Text = "";
            // 
            // weightTextBox
            // 
            this.weightTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.weightTextBox.Location = new System.Drawing.Point(598, 322);
            this.weightTextBox.Name = "weightTextBox";
            this.weightTextBox.Size = new System.Drawing.Size(124, 37);
            this.weightTextBox.TabIndex = 8;
            this.weightTextBox.Text = "";
            // 
            // imageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(734, 711);
            this.Controls.Add(this.weightTextBox);
            this.Controls.Add(this.graphTextBox);
            this.Controls.Add(this.grafoButton);
            this.Controls.Add(this.titleTextBox);
            this.Controls.Add(this.centersListBox);
            this.Controls.Add(this.selectImageButton);
            this.Controls.Add(this.showCentroidsButton);
            this.Controls.Add(this.imagePictureBox);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Font = new System.Drawing.Font("Segoe UI Light", 9.75F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(750, 750);
            this.MinimumSize = new System.Drawing.Size(700, 700);
            this.Name = "imageForm";
            this.Opacity = 0.92D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Centroides";
            ((System.ComponentModel.ISupportInitialize)(this.imagePictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox imagePictureBox;
        private System.Windows.Forms.Button showCentroidsButton;
        private System.Windows.Forms.Button selectImageButton;
        private System.Windows.Forms.ListBox centersListBox;
        private System.Windows.Forms.TextBox titleTextBox;
        private System.Windows.Forms.OpenFileDialog selectImageFileDialog;
        private System.Windows.Forms.Button grafoButton;
        private System.Windows.Forms.RichTextBox graphTextBox;
        private System.Windows.Forms.RichTextBox weightTextBox;
    }
}

