﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Centroids
{
    public class Graph
    {
        List<Vertex> vertexList;
        public Graph(List<Point> pointList)
        {
            vertexList = new List<Vertex>();
            int counter = 0;
            foreach (Point point in pointList)
            {
                counter++;
                Vertex vertex = new Vertex(point, counter);
                this.vertexList.Add(vertex);
            }

            foreach (Vertex origin in this.vertexList)
            {
                foreach (Vertex destination in this.vertexList)
                {
                    if (origin != destination)
                    {
                        double weight = distance(origin.getPoint(), destination.getPoint());
                        origin.insertAdjacent(destination, weight);
                    }
                }
            }
        }

        public double distance(Point initialPoint, Point finalPoint)
        {
            double distance;
            distance = Math.Sqrt(Math.Pow((finalPoint.X - initialPoint.X), 2) + Math.Pow((finalPoint.Y - initialPoint.Y), 2));
            return distance;
        }

        public List<Vertex> getVertexList()
        {
            return this.vertexList;
        }
    }

    public class Vertex
    {
        Point point;
        int name;
        List<Edge> edgeList;
        
        public Vertex(Point point, int counter)
        {
            this.point = point;
            this.name = counter;
            this.edgeList = new List<Edge>();
        }

        public void insertAdjacent(Vertex vertex, double weight)
        {
            Edge adjacent = new Edge(vertex, weight);
            this.edgeList.Add(adjacent);
        }

        public Point getPoint()
        {
            return this.point;
        }

        public List<Edge> getEdgeList()
        {
            return this.edgeList;
        }

        public int getName()
        {
            return this.name;
        }
    }

    public class Edge
    {
        double weight;
        Vertex vertex;

        public Edge(Vertex vertex, double weight)
        {
            this.vertex = vertex;
            this.weight = weight;
        }

        public Vertex getVertex()
        {
            return this.vertex;
        }

        public double getWeight()
        {
            return this.weight;
        }
    }

    class Algorithm
    {
        public static string graphInfo;
        public double leastHeavy = Double.MaxValue;

        public List<Vertex> lighterWay(Graph graph)
        {
            graphInfo = "";
            List<Vertex> route = new List<Vertex>();
            List<Vertex> lighterWay = new List<Vertex>();
            double totalWeight = 0;
            int counter = 0;
            
            if (graph.getVertexList().Count >= 4)
            {
                foreach (Vertex vertex in graph.getVertexList())
                {
                    route.Clear();
                    route.Add(vertex);

                    graphInfo += (vertex.getName().ToString() + " -> ");

                    foreach (Edge edge in vertex.getEdgeList())
                    {
                        var weight = decimal.Truncate(System.Convert.ToDecimal(edge.getWeight()));
                        graphInfo += (": " + weight.ToString() + ": ");
                        graphInfo += (edge.getVertex().getName().ToString() + " -> ");

                        Vertex destiny = edge.getVertex();
                        route.Add(destiny);
                        totalWeight += edge.getWeight();

                        counter++;
                        if(counter == vertex.getEdgeList().Count())
                        {
                            graphInfo += "\n";
                            counter = 0;
                        }
                            
                        foreach (Edge secondEdge in destiny.getEdgeList())
                        {
                            Vertex secondDestiny = secondEdge.getVertex();
                            if (secondDestiny != vertex)
                            {
                                totalWeight += secondEdge.getWeight();
                                route.Add(secondDestiny);

                                foreach (Edge thirdEdge in secondDestiny.getEdgeList())
                                {
                                    Vertex thirdDestiny = thirdEdge.getVertex();
                                    if (thirdDestiny != vertex && thirdDestiny != destiny)
                                    {
                                        totalWeight += thirdEdge.getWeight();
                                        route.Add(thirdDestiny);
                                    }

                                    if (route.Count >= 4)
                                    {
                                        if (totalWeight < leastHeavy)
                                        {
                                            lighterWay = route.ToList();
                                            leastHeavy = totalWeight;
                                            
                                        }
                                        route.RemoveAt(route.Count - 1);
                                        totalWeight -= thirdEdge.getWeight();
                                    }
                                }
                                route.RemoveRange(2, 1);
                                totalWeight -= secondEdge.getWeight();
                            }
                        }
                        route.RemoveRange(1, 1);
                        totalWeight -= edge.getWeight();
                    }
                }
            }
            return lighterWay;
        }
    }
}