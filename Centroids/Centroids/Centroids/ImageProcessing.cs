﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Centroids
{
    class ImageProcessing
    {
        Bitmap originalImage;
        Bitmap backupImage;

        public ImageProcessing(Bitmap bitmap)
        {
            originalImage = bitmap;
            backupImage = new Bitmap(originalImage);
        }

        public List<Point> FindCircles()
        {
            List<Point> centers = new List<Point>();
            for (int i = 0; i < backupImage.Width; i++)
                for (int j = 0; j < backupImage.Height; j++)
                {
                    if (backupImage.GetPixel(i, j).R == 0)
                        if (backupImage.GetPixel(i, j).G == 0)
                            if (backupImage.GetPixel(i, j).B == 0)
                            {
                                Point point = new Point();
                                point = FindCenter(i, j);
                                centers.Add(point);
                                DrawCircle(point, Radius(point));
                                DrawCenter(point);
                            }
                }
            return centers;
        }

        public int Radius(Point centroid)
        {
            int x, y, radius = 0;
            y = centroid.Y;
            for (x = centroid.X; x < backupImage.Width; x++)
            {
                radius++;
                if (backupImage.GetPixel(x, y).R != 0)
                    if (backupImage.GetPixel(x, y).G != 0)
                        if (backupImage.GetPixel(x, y).B != 0)
                            break;
            }
            radius--;
            return radius;
        }

        public Point FindCenter(int initialX, int initialY)
        {
            int centralX = 1, x = initialX, finalX;
            int centralY = 1, y = initialY, finalY;
            for (finalX = initialX; finalX < backupImage.Width; finalX++)
            {
                if (backupImage.GetPixel(finalX, y).R != 0)
                    if (backupImage.GetPixel(finalX, y).G != 0)
                        if (backupImage.GetPixel(finalX, y).B != 0)
                            break;
            }
            finalX--;

            for (finalY = initialY; finalY < backupImage.Width; finalY++)
            {

                if (backupImage.GetPixel(x, finalY).R != 0)
                    if (backupImage.GetPixel(x, finalY).G != 0)
                        if (backupImage.GetPixel(x, finalY).B != 0)
                            break;
            }

            finalY--;
            centralX = (finalX + initialX) / 2;
            centralY = (finalY + initialY) / 2;
            return new Point(centralX, centralY);
        }

        private void DrawCircle(Point point, int radius)
        {
            int initialX, initialY;
            initialX = point.X - radius - 6;
            initialY = point.Y - radius - 6;
            Graphics graphics = Graphics.FromImage(backupImage);
            SolidBrush brush = new SolidBrush(Color.Red);
            graphics.FillEllipse(brush, new Rectangle(initialX, initialY, (2 * radius) + 12, (2 * radius) + 12));
        }

        private void DrawCenter(Point point)
        {
            Graphics graphics = Graphics.FromImage(backupImage);
            graphics.DrawEllipse(new Pen(Color.Blue, 2), point.X, point.Y, 3, 3);
        }

        public Bitmap GetPicture()
        {
            return backupImage;
        }
    }
}
